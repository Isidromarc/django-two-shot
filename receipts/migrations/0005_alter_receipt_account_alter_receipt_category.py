# Generated by Django 4.2 on 2023-04-19 23:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0004_alter_receipt_account_alter_receipt_category"),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="account",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="receipts",
                to="receipts.account",
            ),
        ),
        migrations.AlterField(
            model_name="receipt",
            name="category",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="receipts",
                to="receipts.expensecategory",
            ),
        ),
    ]
