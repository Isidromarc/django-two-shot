from django import forms
from django.contrib.auth import authenticate, get_user_model


class LoginForm(forms.Form):
    username = forms.CharField(label="Username", max_length=150, required=True)
    password = forms.CharField(
        label="Password", widget=forms.PasswordInput, required=True
    )

    class Meta:
        model = get_user_model()
        fields = ["username", "password"]

    def clean(self):
        username = self.cleaned_data["username"]
        password = self.cleaned_data["password"]

        if not authenticate(username=username, password=password):
            raise forms.ValidationError("Incorrect username or password.")


class RegisterForm(forms.ModelForm):
    username = forms.CharField(label="Username", max_length=150, required=True)
    password = forms.CharField(
        label="Password",
        max_length=150,
        required=True,
        widget=forms.PasswordInput,
    )
    password_confirmation = forms.CharField(
        label="password_confirmation",
        max_length=150,
        required=True,
        widget=forms.PasswordInput,
    )

    class Meta:
        model = get_user_model()
        fields = ["username", "password"]

    def clean(self):
        username = self.cleaned_data["username"]
        password1 = self.cleaned_data["password"]
        password2 = self.cleaned_data["password_confirmation"]

        errors = []

        if password1 != password2:
            errors.append("Passwords do not match.")

        # if get_user_model().objects.filter(username=username).exists():
        #     errors.append("Username already exists.")

        if errors:
            raise forms.ValidationError(errors)
